using System;
using System.Collections.Generic;
using System.Linq;


using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using XTbsLibrary;

using BaconGameJam06.GameScreens;

namespace BaconGameJam06
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        #region Field Region

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Random random;

        GameStateManager stateManager;
        GamePlayScreen gamePlayScreen;
        MainMenuScreen mainMenuScreen;

        Song backgroundTrack;


        const int screenWidth = 1024;
        const int screenHeight = 600;

        #endregion

        #region Property Region

        public readonly Rectangle ScreenRectangle;

        public SpriteBatch SpriteBatch
        {
            get { return spriteBatch; }
        }

        public Random Random
        {
            get { return random; }
        }

        #endregion

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferHeight = screenHeight;
            graphics.PreferredBackBufferWidth = screenWidth;
            graphics.ApplyChanges();
            IsMouseVisible = true;

            this.Window.AllowUserResizing = false;
            this.Window.Title = "BGJ06: Walk the Rainbow";

            ScreenRectangle = new Rectangle(0, 0, screenWidth, screenWidth);

            Components.Add(new InputHandler(this));

            Content.RootDirectory = "Content";
            random = new Random();


            stateManager = new GameStateManager(this);
            gamePlayScreen = new GamePlayScreen(this, stateManager);
            mainMenuScreen = new MainMenuScreen(this, stateManager);
            //stateManager.ChangeState(gamePlayScreen);
            stateManager.ChangeState(mainMenuScreen);
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            backgroundTrack = Content.Load<Song>(@"Music/Disco con Tutti");
            MediaPlayer.Play(backgroundTrack);
            MediaPlayer.IsRepeating = true;
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();
            if ((InputHandler.KeyDown(Keys.LeftAlt) || InputHandler.KeyDown(Keys.RightAlt)) && InputHandler.KeyPressed(Keys.F4))
                this.Exit();
            // TODO: Add your update logic here
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            //GraphicsDevice.Clear(Color.CornflowerBlue);
            GraphicsDevice.Clear(new Color(15, 15, 15)); // Deep Charcoal Grey

            // TODO: Add your drawing code here
            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
            base.Draw(gameTime);
            spriteBatch.End();
        }
    }
}
