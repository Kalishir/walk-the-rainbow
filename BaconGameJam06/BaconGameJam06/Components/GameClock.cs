﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using XTbsLibrary;
using XTbsLibrary.Controls;

namespace BaconGameJam06.Components
{
    public class GameClock : DrawableGameComponent
    {
        public event EventHandler OutOfTime;

        const int initialTime = 60;
        const int bonusTime = 15;

        public bool ZeroTime = false;
        bool running;
        int milliseconds = 0;
        Game1 GameRef;
        SpriteFont font;
        Vector2 screenPos;
        
        public GameClock(Game game)
            : base(game)
        {
            GameRef = (Game1)Game;
            font = ControlManager.SpriteFont;
            AddTime(initialTime);
            running = true;
        }

        public void AddTime(int seconds)
        {
            milliseconds += seconds * 1000;
        }

        public override void Update(GameTime gameTime)
        {
        #if DEBUG
            if (InputHandler.KeyPressed(Keys.Escape))
                StartStop();
            if(InputHandler.KeyPressed(Keys.F1))
                milliseconds = 0;
        #endif

            base.Update(gameTime);
            
            if(running)
                milliseconds -= gameTime.ElapsedGameTime.Milliseconds;
            if (milliseconds <= 0 && !ZeroTime)
                OnOutOfTime(null);
        }
        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
            string temp = "Time Remaining: " + (milliseconds / 1000);
            screenPos = new Vector2(830 - font.MeasureString(temp).X/2, 460);
            GameRef.SpriteBatch.DrawString(font, temp, screenPos + new Vector2(1, 0), Color.White);
            GameRef.SpriteBatch.DrawString(font, temp, screenPos, Color.White);
        }

        public void StartStop()
        {
            running = running ^ true;
        }

        protected void OnOutOfTime(EventArgs e)
        {
            ZeroTime = true;
            if (OutOfTime != null)
                OutOfTime(this, e);
        }

        public void Reset()
        {
            milliseconds = 0;
            AddTime(initialTime);
        }
    }
}
