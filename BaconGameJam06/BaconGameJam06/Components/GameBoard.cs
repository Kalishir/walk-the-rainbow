﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace BaconGameJam06.Components
{
    enum Directions
    {
        Up = 1,
        Down = 2,
        Left = 4,
        Right = 8
    }

    public class GameBoard : DrawableGameComponent
    {
        #region Field Region

        const float lerpSpeed = 0.1f;

        int[,] board;
        Color[,] colors;
        Color[,] targetColor;
        int width;
        int height;
        Vector2 boardPosition;
        Texture2D blankTile;

        #endregion

        #region Property Region
        public int Width
        {
            get { return width; }
        }

        public int Height
        {
            get { return width; }
        }
        #endregion

        #region Constructor Region

        public GameBoard(int boardWidth, int boardHeight, Vector2 boardPos, Game game)
            :base(game)
        { 
            board = new int[boardWidth, boardHeight];
            colors = new Color[boardWidth, boardHeight];
            targetColor = new Color[boardWidth, boardHeight];
            boardPosition = boardPos;
            width = boardWidth;
            height = boardHeight;
            
            GenerateBoard();

            blankTile = game.Content.Load<Texture2D>("BlankTile");
        }

        #endregion

        #region XNA Method Region

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    colors[x, y] = Color.Lerp(colors[x, y], targetColor[x, y], lerpSpeed);
                }
            }
        }


        public override void Draw(GameTime gameTime)
        {
            for (int x = 0; x < width; x++)
		    {
			    for(int y = 0; y < height; y++)
			    {
				    DrawTile(board[x,y], x, y);
			    }
		    }
        }

        #endregion

        #region Method Region

        public void Reset()
        {
            GenerateBoard();

        }
        
        private void GenerateBoard()
        {
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    board[x,y] = ((Game1)Game).Random.Next(6);
                    colors[x, y] = Color.White;
                    targetColor[x, y] = intToColor(board[x, y]);
                }
            }
        }

        public int getTile(Vector2 loc)
        {
            return board[(int)loc.X, (int)loc.Y];
        }

        private Color intToColor(int value)
        {
            switch (value)
            {
                case 0:
                    return new Color(176, 0, 0);
                case 1:
                    return Color.DarkOrange;
                case 2:
                    return new Color(239, 239, 0);
                case 3:
                    return Color.Green;
                case 4:
                    return new Color(0, 0, 176);
                case 5:
                    return Color.Indigo;
                default:
                    throw new Exception("Index out of Range");
            }
        }

        public void DrawTile(int color, int x, int y)
        {
            Vector2 ScreenPos = GridToScreen(x, y);
            ((Game1)Game).SpriteBatch.Draw(blankTile, ScreenPos, colors[x,y]);
        }

        public Vector2 GridToScreen(int x, int y)
        {
            float ScreenX = boardPosition.X + x * blankTile.Bounds.Width;
            float ScreenY = boardPosition.Y + y * blankTile.Bounds.Height;
            return new Vector2(ScreenX, ScreenY);
        }

        public Vector2 GridToScreen(Vector2 gridCoords)
        {
            float ScreenX = boardPosition.X + gridCoords.X * blankTile.Bounds.Width;
            float ScreenY = boardPosition.Y + gridCoords.Y * blankTile.Bounds.Height;
            return new Vector2(ScreenX, ScreenY);
        }

        public Color GetTileColor(Vector2 loc)
        {
            return intToColor(board[(int)loc.X, (int)loc.Y]);
        }

        public void GenerateNewTile(Vector2 loc)
        {
            int newColor = ((Game1)Game).Random.Next(5);
            if (newColor >= board[(int)loc.X, (int)loc.Y])
                newColor++;
            board[(int)loc.X, (int)loc.Y] = newColor;
            targetColor[(int)loc.X, (int)loc.Y] = intToColor(newColor);
        }

        #endregion
    }
}
