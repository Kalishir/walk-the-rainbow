﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using XTbsLibrary.Controls;


namespace BaconGameJam06.Components
{
    public struct ScoreStats
    {
        public int total;
        public int multiplier;
        public int highestChain;
        public int highestMultiplier;
        public int previousTotal;

        public ScoreStats(ScoreStats s)
        {
            total = s.total;
            multiplier = s.multiplier;
            highestChain = s.highestChain;
            highestMultiplier = s.highestMultiplier;
            previousTotal = s.previousTotal;
        }

        public override string ToString()
        {
            return String.Format("Total: {0,22}\n\nHighest Chain: {1,9}\n\nHighest Multiplier: {2,3}", total, highestChain, highestMultiplier);
        }
    }

    public class Score : DrawableGameComponent
    {
        ScoreStats stats;

        SpriteFont font;
        Game1 GameRef;
        Vector2 screenPos;

        public ScoreStats Stats
        {
            get { return new ScoreStats(stats); }
        }

        public Score(Game game)
            :base(game)
        {
            stats = new ScoreStats() { multiplier = 1 };

            GameRef = (Game1)game;
            font = ControlManager.SpriteFont;
            screenPos = new Vector2(830, 60);
        }

        public void Reset()
        {
            stats = new ScoreStats() { multiplier = 1 };
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
            string total = String.Format("Total: {0:0000} ({1:0000})", stats.total, stats.total - stats.previousTotal);
            string multi = String.Format("Multiplier: {0:00}", stats.multiplier);
            string highestChain = String.Format("Highest Chain: {0:000}", stats.highestChain);
            string highestMulti = String.Format("Highest Multiplier: {0:00}", stats.highestMultiplier);

            Vector2 pos = screenPos;
            pos.X -= font.MeasureString(total).X / 2;
            GameRef.SpriteBatch.DrawString(font, total, pos + new Vector2(1, 0), Color.White);
            GameRef.SpriteBatch.DrawString(font, total, pos, Color.White);

            pos.X = screenPos.X - font.MeasureString(multi).X / 2;
            pos.Y += 100;
            GameRef.SpriteBatch.DrawString(font, multi, pos + new Vector2(1, 0), Color.White);
            GameRef.SpriteBatch.DrawString(font, multi, pos, Color.White);

            pos.X = screenPos.X - font.MeasureString(highestChain).X / 2;
            pos.Y += 100;
            GameRef.SpriteBatch.DrawString(font, highestChain, pos + new Vector2(1, 0), Color.White);
            GameRef.SpriteBatch.DrawString(font, highestChain, pos, Color.White);

            pos.X = screenPos.X - font.MeasureString(highestMulti).X / 2;
            pos.Y += 100;
            GameRef.SpriteBatch.DrawString(font, highestMulti, pos + new Vector2(1, 0), Color.White);
            GameRef.SpriteBatch.DrawString(font, highestMulti, pos, Color.White);
        }

        public void UpdateScore(MoveArgs e)
        {
            UpdateMultiplier(e.Prev, e.Next);
            stats.total += stats.multiplier;
        }

        private void UpdateMultiplier(int prev, int next)
        {
            if (prev == next)
                return;


            if ((prev == next - 1 || prev == next + 1) ||
                    (prev == 0 && next == 5) ||
                    (prev == 5 && next == 0))
                stats.multiplier++;
            else
            {
                if (stats.multiplier > stats.highestMultiplier)
                    stats.highestMultiplier = stats.multiplier;

                stats.multiplier = 1;

                int currentChain = stats.total - stats.previousTotal;
                stats.previousTotal = stats.total;
                if (currentChain > stats.highestChain)
                    stats.highestChain = currentChain;
            }
        }
    }
}
