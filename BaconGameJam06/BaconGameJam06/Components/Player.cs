﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

using XTbsLibrary;

namespace BaconGameJam06.Components
{
    public class MoveArgs : EventArgs
    {
        public int Prev;
        public int Next;

        public MoveArgs(int p, int n)
        {
            Prev = p;
            Next = n;
        }
    }

    class Player : DrawableGameComponent
    {
        public delegate void ScoreEventHandler(MoveArgs e);
        public event ScoreEventHandler HasMoved;

        #region Field Region

        const float fadeSpeed = 0.05f;
        Vector2 gridPosition;
        GameBoard boardRef;
        Texture2D playerSprite;
        int red;
        int green;
        int blue;

        #endregion

        #region Property Region

        public Color TargetColor;

        #endregion

        #region Constructor Region

        public Player(GameBoard board, Game game)
            : base(game)
        {
            boardRef = board;
            GeneratePlayerPosition();
            playerSprite = Game.Content.Load<Texture2D>("PlayerToken");
            red = 255;
            green = 255;
            blue = 255;
        }

        #endregion

        #region XNA Method Region

        protected override void LoadContent()
        {
            base.LoadContent();
        }
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            red = (int)MathHelper.Lerp(red, TargetColor.R, fadeSpeed);
            green = (int)MathHelper.Lerp(green, TargetColor.G, fadeSpeed);
            blue = (int)MathHelper.Lerp(blue, TargetColor.B, fadeSpeed);
            HandleInput();
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
            ((Game1)Game).SpriteBatch.Draw(playerSprite, boardRef.GridToScreen(gridPosition), new Color(red, green, blue));
        }

        #endregion

        #region Method Region

        public void Reset()
        {
            red = 255;
            green = 255;
            blue = 255;
            GeneratePlayerPosition();
        }

        private void GeneratePlayerPosition()
        {
            int x = ((Game1)Game).Random.Next(boardRef.Width);
            int y = ((Game1)Game).Random.Next(boardRef.Height);
            gridPosition = new Vector2(x, y);
            TargetColor = boardRef.GetTileColor(gridPosition);
        }

        public void HandleInput()
        {
            if (InputHandler.KeyPressed(Keys.NumPad1))
                MovePlayer(Directions.Down | Directions.Left);
            if (InputHandler.KeyPressed(Keys.NumPad2))
                MovePlayer(Directions.Down);
            if (InputHandler.KeyPressed(Keys.NumPad3))
                MovePlayer(Directions.Down | Directions.Right);
            if (InputHandler.KeyPressed(Keys.NumPad4))
                MovePlayer(Directions.Left);
            if (InputHandler.KeyPressed(Keys.NumPad6))
                MovePlayer(Directions.Right);
            if (InputHandler.KeyPressed(Keys.NumPad7))
                MovePlayer(Directions.Up | Directions.Left);
            if (InputHandler.KeyPressed(Keys.NumPad8))
                MovePlayer(Directions.Up);
            if (InputHandler.KeyPressed(Keys.NumPad9))
                MovePlayer(Directions.Up | Directions.Right);
        }

        public void MovePlayer(Directions dir)
        {
            int x = 0;
            int y = 0;
            if (dir.HasFlag(Directions.Up))         y -= 1;
            if (dir.HasFlag(Directions.Down))       y += 1;
            if (dir.HasFlag(Directions.Right))      x += 1;
            if (dir.HasFlag(Directions.Left))       x -= 1;

            if (gridPosition.X + x < boardRef.Width
                && gridPosition.Y + y < boardRef.Height
                && gridPosition.X + x >= 0
                && gridPosition.Y + y >= 0)
            {
                int prev = boardRef.getTile(gridPosition);
                boardRef.GenerateNewTile(gridPosition);
                gridPosition += new Vector2(x, y);
                TargetColor = boardRef.GetTileColor(gridPosition);
                OnHasMoved(new MoveArgs(prev, boardRef.getTile(gridPosition)));
            }
        }

        protected void OnHasMoved(MoveArgs e)
        {
            if (HasMoved != null)
                HasMoved(e);
        }

        #endregion
    }
}
