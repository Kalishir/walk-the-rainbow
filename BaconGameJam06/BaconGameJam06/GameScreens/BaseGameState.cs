﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

using XTbsLibrary;
using XTbsLibrary.Controls;

namespace BaconGameJam06.GameScreens
{
    public abstract partial class BaseGameState : GameState
    {
        #region Fields Region

        protected Game1 GameRef;

        protected ControlManager ControlManager;

        #endregion

        #region Property Region
        #endregion

        #region Constructor Region

        public BaseGameState(Game game, GameStateManager manager)
            : base(game, manager)
        {
            GameRef = (Game1)game;
        }

        #endregion

        #region XNA Methods Region

        protected override void LoadContent()
        {
            ContentManager Content = Game.Content;

            SpriteFont menuFont = Content.Load<SpriteFont>(@"Fonts\ControlFont");

            ControlManager = new ControlManager(menuFont);

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            ControlManager.Update(gameTime, PlayerIndex.One);
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }

        #endregion

        #region Methods Region
        #endregion
    }
}
