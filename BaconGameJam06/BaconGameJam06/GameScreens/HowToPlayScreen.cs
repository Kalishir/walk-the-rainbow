﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using XTbsLibrary;

namespace BaconGameJam06.GameScreens
{
    public class HowToPlayScreen : BaseGameState
    {
        Texture2D image;

        public HowToPlayScreen(Game game, GameStateManager stateManager)
            : base(game, stateManager)
        { 

        }

        protected override void LoadContent()
        {
            base.LoadContent();

            image = GameRef.Content.Load<Texture2D>("howtoplayscreen");
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            if (InputHandler.KeyPressed(Microsoft.Xna.Framework.Input.Keys.Escape))
                StateManager.PopState();
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);

            GameRef.SpriteBatch.Draw(image, image.Bounds, Color.White);
        }
    }
} 
