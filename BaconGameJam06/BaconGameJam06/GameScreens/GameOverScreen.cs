﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

using BaconGameJam06.Components;

using XTbsLibrary;

namespace BaconGameJam06.GameScreens
{
    public class GameOverScreen : BaseGameState
    {

        Texture2D background;
        Texture2D panel;
        ScoreStats lastGameStats;
        SpriteFont font;

        public GameOverScreen(Game game, GameStateManager stateManager, ScoreStats stats)
            : base(game, stateManager)
        {
            background = GameRef.Content.Load<Texture2D>("rainbowBackground");
            panel = GameRef.Content.Load<Texture2D>("ColorTilePanel");
            lastGameStats = stats;
            font = XTbsLibrary.Controls.ControlManager.SpriteFont;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            if (InputHandler.KeyPressed(Microsoft.Xna.Framework.Input.Keys.Escape))
                StateManager.PopState();
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);

            Vector2 panelPos = new Vector2(
                        GameRef.GraphicsDevice.Viewport.Width / 2 - panel.Width / 2,
                        GameRef.GraphicsDevice.Viewport.Height / 2 - panel.Height / 2
                );
            GameRef.SpriteBatch.Draw(background, background.Bounds, Color.White);
            GameRef.SpriteBatch.Draw(panel, panelPos, Color.White);

            string gameOverText = "Game Over!";
            Vector2 gameOver = new Vector2(
                        GameRef.GraphicsDevice.Viewport.Width / 2 - font.MeasureString(gameOverText.ToString()).X / 2,
                        90);

            GameRef.SpriteBatch.DrawString(font, gameOverText, gameOver + new Vector2(1,0), Color.Red);
            GameRef.SpriteBatch.DrawString(font, gameOverText, gameOver, Color.Red);


            Vector2 screenPos = new Vector2(
                        GameRef.GraphicsDevice.Viewport.Width / 2 - font.MeasureString(lastGameStats.ToString()).X / 2,
                        200);
            GameRef.SpriteBatch.DrawString(font, lastGameStats.ToString(), screenPos + new Vector2(1, 0), Color.White);
            GameRef.SpriteBatch.DrawString(font, lastGameStats.ToString(), screenPos, Color.White);


            string escape = "Press ESCAPE for menu.";
            Vector2 escapePos = new Vector2(
                        GameRef.GraphicsDevice.Viewport.Width / 2 - font.MeasureString(escape).X / 2,
                        445);
            GameRef.SpriteBatch.DrawString(font, escape, escapePos + new Vector2(1, 0), Color.Red);
            GameRef.SpriteBatch.DrawString(font, escape, escapePos, Color.Red);
        }
    }
}
