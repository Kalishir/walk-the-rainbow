﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using XTbsLibrary;

using BaconGameJam06.Components;


namespace BaconGameJam06.GameScreens
{
    public class GamePlayScreen : BaseGameState
    {
        #region Fields Region

        GameBoard gameBoard;
        Player player;
        Score score;
        GameClock timer;

        Texture2D background;
        Texture2D panel;

        #endregion

        #region Property Region
        #endregion

        #region Constructor Region

        public GamePlayScreen(Game game, GameStateManager manager)
            : base(game, manager)
        {
            GameRef = (Game1)game;
        }

        #endregion

        #region XNA Methods Region

        protected override void LoadContent()
        {
            base.LoadContent();

            background = GameRef.Content.Load<Texture2D>("rainbowBackground");
            panel = GameRef.Content.Load<Texture2D>("ColorTilePanel");
            gameBoard = new GameBoard(10, 10, new Vector2(100, 50), GameRef);
            player = new Player(gameBoard, GameRef);
            score = new Score(GameRef);
            player.HasMoved += score.UpdateScore;
            timer = new GameClock(GameRef);
            timer.OutOfTime += new EventHandler(timer_OutOfTime);
            ChildComponents.Add(gameBoard);
            ChildComponents.Add(player);
            ChildComponents.Add(score);
            ChildComponents.Add(timer);
        }

        public override void Update(GameTime gameTime)
        {
            #if DEBUG
                if (InputHandler.KeyPressed(Keys.F2)
            #else
                if (InputHandler.KeyPressed(Keys.Escape)
            #endif
                    || timer.ZeroTime)
            {
                StateManager.PopState();
            }

                base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            GameRef.SpriteBatch.Draw(background, background.Bounds, Color.Gray);
            GameRef.SpriteBatch.Draw(panel, new Vector2(680, 50), Color.White);
            base.Draw(gameTime);
        }

        #endregion

        #region Methods Region

        void timer_OutOfTime(object sender, EventArgs e)
        {
            StateManager.PushState(new GameOverScreen(GameRef, StateManager, score.Stats));
        }

        #endregion
    }
}
