﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Media;

using XTbsLibrary;
using XTbsLibrary.Controls;

namespace BaconGameJam06.GameScreens
{
    class MainMenuScreen : BaseGameState
    {
        Texture2D background;

        LinkPictureBox newGameButton;
        LinkPictureBox howToPlayButton;
        LinkPictureBox exitGameButton;
        //LinkPictureBox SFXButton;
        LinkPictureBox MusicButton;
        LinkPictureBox MusicButtonDisabled;


        public MainMenuScreen(Game game, GameStateManager stateManager)
            : base(game, stateManager)
        {
        }


        protected override void LoadContent()
        {
            base.LoadContent();

            background = GameRef.Content.Load<Texture2D>("rainbowBackground");


            Texture2D temp = GameRef.Content.Load<Texture2D>(@"MenuButtons/NewGameButton");
            Texture2D tempSelected = GameRef.Content.Load<Texture2D>(@"MenuButtons/NewGameButtonSelected");
            Vector2 screenPos = new Vector2(
                            GameRef.Window.ClientBounds.Width / 2 - temp.Width / 2,
                            100);
            Vector2 padding = new Vector2(0, 150);

            newGameButton = new LinkPictureBox(temp, tempSelected, new Rectangle((int)screenPos.X, (int)screenPos.Y, temp.Width, temp.Height), false);
            newGameButton.Selected += new EventHandler(Button_Selected);
            newGameButton.SelectedColor = Color.White;


            temp = GameRef.Content.Load<Texture2D>(@"MenuButtons/HowToPlayButton");
            tempSelected = GameRef.Content.Load<Texture2D>(@"MenuButtons/HowToPlayButtonSelected");
            screenPos += padding;
            howToPlayButton = new LinkPictureBox(temp, tempSelected, new Rectangle((int)screenPos.X, (int)screenPos.Y, temp.Width, temp.Height), false);
            howToPlayButton.Selected += new EventHandler(Button_Selected);
            howToPlayButton.SelectedColor = Color.White;


            temp = GameRef.Content.Load<Texture2D>(@"MenuButtons/ExitGameButton");
            tempSelected = GameRef.Content.Load<Texture2D>(@"MenuButtons/ExitGameButtonSelected");
            screenPos += padding;
            exitGameButton = new LinkPictureBox(temp, tempSelected, new Rectangle((int)screenPos.X, (int)screenPos.Y, temp.Width, temp.Height), false);
            exitGameButton.Selected += new EventHandler(Button_Selected);
            exitGameButton.SelectedColor = Color.White;


            temp = GameRef.Content.Load<Texture2D>(@"MenuButtons/MusicButton");
            tempSelected = GameRef.Content.Load<Texture2D>(@"MenuButtons/MusicButtonSelected");
            screenPos += padding;
            MusicButton = new LinkPictureBox(temp, temp, new Rectangle(50, 50, temp.Height, temp.Width), false);
            MusicButton.Selected += new EventHandler(Button_Selected);
            MusicButton.Color = Color.AliceBlue;
            MusicButton.SelectedColor = Color.White;
            MusicButton.TabStop = false;

            MusicButtonDisabled = new LinkPictureBox(tempSelected, tempSelected, new Rectangle(50, 50, tempSelected.Height, tempSelected.Width), false);
            MusicButtonDisabled.Selected += new EventHandler(Button_Selected);
            MusicButtonDisabled.Color = Color.AliceBlue;
            MusicButtonDisabled.SelectedColor = Color.White;
            MusicButtonDisabled.TabStop = false;
            MusicButtonDisabled.Visible = false;

            ControlManager.Add(newGameButton);
            ControlManager.Add(howToPlayButton);
            ControlManager.Add(exitGameButton);
            ControlManager.Add(MusicButton);
            ControlManager.Add(MusicButtonDisabled);

            ControlManager.SelectControl(newGameButton);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            HandleInput();
        }


        public override void Draw(GameTime gameTime)
        {
            GameRef.SpriteBatch.Draw(background, background.Bounds, Color.DarkSlateGray);

            base.Draw(gameTime);

            ControlManager.Draw(GameRef.SpriteBatch);
        }

        public void HandleInput()
        {
            if(InputHandler.KeyPressed(Keys.Up))
                ControlManager.PreviousControl();
            if(InputHandler.KeyPressed(Keys.Down))
                ControlManager.NextControl();
            if (InputHandler.KeyPressed(Keys.Left))
                ControlManager.SelectControl(MusicButton);
            if (InputHandler.KeyPressed(Keys.Right))
                ControlManager.NextControl();
        }

        void Button_Selected(object sender, EventArgs e)
        {
            if (sender == newGameButton)
                StateManager.PushState(new GamePlayScreen(GameRef, StateManager));

            if (sender == howToPlayButton)
            {
                StateManager.PushState(new HowToPlayScreen(GameRef, StateManager));
            }

            if (sender == exitGameButton)
                GameRef.Exit();

            if (sender == MusicButton)
            {
                MediaPlayer.Volume = (int)MediaPlayer.Volume ^ 1;
                if (MediaPlayer.Volume > 0.5f)
                    MusicButtonDisabled.Visible = false;
                else
                    MusicButtonDisabled.Visible = true;
            }
        }
    }
}
